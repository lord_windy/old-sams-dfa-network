use std::fs::File;
use std::io::Read;

pub struct Image {
  pub pixels: Vec<f32>,
  pub label: i32,
}

pub struct Dataset {
  pub training: Vec<Image>,
  pub testing: Vec<Image>,
}

impl Dataset {
  
  //data set comes in reverse
  fn generate_set_image(&mut self, image_file: &mut Vec<u8>, label_file: &mut Vec<u8>,
                  image_size: i32) -> Image {
    let mut img = Image {
      pixels: vec![],
      label: -1,
    };
    
    img.label = label_file.pop().unwrap() as i32;
    for _i in 0..image_size as usize {
      img.pixels.push((image_file.pop().unwrap() as f32) / 255.0 )
    }
    img.pixels.reverse(); //pixels now in correct order
    
    img
  }

  fn generate_set(&mut self, image_file: &mut File, label_file: &mut File,
                  data_set_size: i32, image_size: i32) -> Vec<Image> {
    //get our data sets
    let mut image_vector: Vec<u8> = Vec::new();
    image_file.read_to_end(&mut image_vector);
    image_vector.reverse();
    //got waste the first x number of bytes
    println!("Image_vector length: {}", image_vector.len()); 
    for _i in 0..16 {
      image_vector.pop().unwrap();
    }

    let mut label_vector: Vec<u8> = Vec::new();
    label_file.read_to_end(&mut label_vector);
    label_vector.reverse();
    //gotta waste the first x number of bytes
    for _i in 0..8 {
      label_vector.pop().unwrap();
    }

    let mut images = Vec::new();

    for _i in 0..data_set_size as usize {
      images.push(self.generate_set_image(&mut image_vector, 
            &mut label_vector, image_size));
    }
    
    images

  }

  pub fn new() -> Dataset{
    let mut d = Dataset {
      training: Vec::new(),
      testing: Vec::new(),
    };
    
    let mut training_image = File::open("./data/train-images.idx3-ubyte").unwrap();
    let mut training_label = File::open("./data/train-labels.idx1-ubyte").unwrap();
    
    d.training = d.generate_set(&mut training_image, &mut training_label,
        60000, 784);

    let mut testing_image = File::open("./data/t10k-images.idx3-ubyte").unwrap();
    let mut testing_label = File::open("./data/t10k-labels.idx1-ubyte").unwrap();
    d.testing = d.generate_set(&mut testing_image, &mut testing_label, 
        10000, 784);

    d

  }

}
