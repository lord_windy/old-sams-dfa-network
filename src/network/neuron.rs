use rand::prelude::*;
use rand_distr::{StandardNormal, Normal, Distribution, Uniform};
use std::fs::File;
use std::io::Write;

#[allow(dead_code)]
#[derive(Clone)]
pub enum ActivationType{
	SIGMOID,
	RELU,
}

#[allow(dead_code)]
pub struct Neuron {
	neuron_type: ActivationType,
	weights: Vec<f32>,
	eb_matrix_weights: Vec<Vec<f32>>,
	eb_learning_weights: Vec<Vec<f32>>,
	bias: f32,
	eb_matrix_bias: Vec<f32>,
	eb_learning_bias: Vec<f32>,
	near_output: f32,
	learning_holding: Vec<f32>,
	output: f32,
}

fn dfa_matrix_line(dfa_line: &Vec<f32>, inputs: &Vec<f32>) -> f32 {
	let mut sum = 0.0;

	let len = inputs.len();
	for i in 0..len {
		sum += dfa_line[i] * inputs[i];
	}

	sum
}

pub fn dfa_learn_weight_line(weights: &mut Vec<f32>, learning_weights: &Vec<f32>,learning: f32) {
	let len = weights.len();
	for i in 0..len {
		let sum = learning * learning_weights[i];
		weights[i] -= sum;
	}
}

#[allow(dead_code)]
impl Neuron {
	pub fn new(neuron_type: ActivationType, inputs: i32) -> Neuron {
		let mut n = Neuron {
			neuron_type,
			weights: Vec::new(),
			eb_matrix_weights: Vec::new(),
			eb_learning_weights: Vec::new(),
			bias: 0.0,
			eb_matrix_bias: Vec::new(),
			eb_learning_bias: Vec::new(),
			near_output: 0.0,
			learning_holding: Vec::new(),
			output: 0.0,
		};
    let normal = Normal::new(0.0, 0.2).unwrap();
		for _i in 0..inputs {
			n.weights.push(thread_rng().sample(normal));
		}
		n.bias = thread_rng().sample(normal);

		n
	}

	pub fn get_output(&self) -> f32 {
		return self.output
	}

	fn sum(&self, inputs: &Vec<f32>) -> f32 {
		let mut s = 0.0;

		for i in 0..inputs.len() {
			s += self.weights[i] * inputs[i];
		}

		s += self.bias;

		s
	}

	fn activation_function(&self) -> f32 {
		match self.neuron_type {
			ActivationType::SIGMOID => {
				1.0 / (1.0 + (-1.0 * self.near_output).exp())
          /*std::f32::consts::E.powf(self.near_output * -1.0))*/
			},
			ActivationType::RELU => {
				self.near_output.max(0.0)
			},
		}

	}

	pub fn activate(&mut self, inputs: &Vec<f32>) {
		self.near_output = self.sum(inputs);
		self.output = self.activation_function();
	}

	fn generate_noise(&mut self, inputs: &Vec<f32>) {
		self.eb_matrix_weights = Vec::new();
		self.eb_matrix_bias = Vec::new();
		let weight_len = self.weights.len();
		let input_len = inputs.len();
     
		for _j in 0..weight_len {
			let mut noise = Vec::new();
			for _i in 0..input_len {
				noise.push(thread_rng().sample(StandardNormal));
			}
			self.eb_matrix_weights.push(noise);
		}

		for _i in 0..input_len {
			self.eb_matrix_bias.push(thread_rng().sample(StandardNormal));
		}

	}

  fn dfa_calc_de_output(&mut self) -> f32 {
    
		let de_output; 
    match self.neuron_type {
      ActivationType::SIGMOID => {
        de_output = self.output * ( 1.0 - self.output);
      },
      ActivationType::RELU => {
        if self.output > 0.0 {
          de_output = 1.0;
        } else {
          de_output = 0.0;
        }
      }
    }

    de_output
  }

	fn dfa_exec_weights(&mut self, errors: &Vec<f32>, inputs: &Vec<f32>) -> Vec<f32> {

		let de_output = self.dfa_calc_de_output(); 
		let len = self.weights.len();

		let mut learning = Vec::new();

		for i in 0..len {
			let sum = dfa_matrix_line(&self.eb_matrix_weights[i], errors);
			learning.push(inputs[i] * sum * de_output);
		}

		learning
	}

	fn dfa_exec_bias(&mut self, errors: &Vec<f32>) -> f32 {

		let de_output = self.dfa_calc_de_output(); 
		let sum = dfa_matrix_line(&self.eb_matrix_bias, errors);

		sum * de_output

	}

	pub fn dfa_exec(&mut self, errors: &Vec<f32>, inputs: &Vec<f32>) {
		self.generate_noise(errors);

		let simple = self.dfa_exec_weights(errors, inputs);
		self.eb_learning_weights.push(simple);
		let simple2 = self.dfa_exec_bias(errors);

		self.eb_learning_bias.push(simple2);

	}

	pub fn dfa_learn(&mut self, learning: f32) {

		let mut len = self.eb_learning_weights.len();
		for i in 0..len {
			dfa_learn_weight_line(&mut self.weights,&self.eb_learning_weights[i], learning);
		}
    len = self.eb_learning_bias.len();
    for i in 0..len {
      let sum = self.eb_learning_bias[i] * learning;
      self.bias -= sum;
    }

		self.eb_learning_weights = Vec::new();
    self.eb_learning_bias = Vec::new();

	}

	pub fn debug_init(&self, file: &mut File, layer: i32, neuron: i32) {
		let len = self.weights.len();
		for i in 0..len {
			file.write_fmt(format_args!("Layer {} Neuron {} Weight {}, ", layer, neuron, i)).unwrap();
		}
		file.write_fmt(format_args!("Layer {} Neuron {} Bias, ", layer, neuron)).unwrap();
		file.write_fmt(format_args!("Layer {} Neuron {} Near Output, ", layer, neuron)).unwrap();
		file.write_fmt(format_args!("Layer {} Neuron {} Output, ", layer, neuron)).unwrap();
	}

	pub fn debug_out(&self, file: &mut File) {
		let len = self.weights.len();
		for i in 0..len {
			file.write_fmt(format_args!("{}, ", self.weights[i])).unwrap();
		}

		file.write_fmt(format_args!("{}, ", self.bias)).unwrap();
		file.write_fmt(format_args!("{}, ", self.near_output)).unwrap();
		file.write_fmt(format_args!("{}, ", self.output)).unwrap();
	}

}
