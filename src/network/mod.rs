use crate::network::neuron::{Neuron, ActivationType};
use std::fs::File;
use std::io::Write;

use ocl::ProQue;

pub mod neuron;

pub enum ErrorType{
	MSE,
}

pub struct Network {
	neurons: Vec<Vec<Neuron>>
}

fn activate_layer(neurons: &mut Vec<Neuron>, inputs: &Vec<f32>) {
	let len = neurons.len();
	for i in 0..len {
		neurons[i].activate(inputs);
	}
}

fn outputs_layer(neurons: &Vec<Neuron>) -> Vec<f32> {
	let len = neurons.len();
	let mut outputs = Vec::new();
	for i in 0..len {
		outputs.push(neurons[i].get_output());
	}

	outputs
}

fn debug_init_layer(neurons: &Vec<Neuron>, file: &mut File, layer: i32) {
	let len = neurons.len();
	for i in 0..len {
		neurons[i].debug_init(file, layer, i as i32);
	}
}

fn debug_out_layer(neurons: &Vec<Neuron>, file: &mut File) {
	let len = neurons.len();
	for i in 0..len {
		neurons[i].debug_out(file);
	}
}

fn mse_error(outputs: &Vec<f32>, expected: i32) -> f32 {
	let len = outputs.len();
	let mut error: f32 = 0.0;

	for i in 0..len {
		let to_minus;
		if i as i32 == expected {
			to_minus = 1.0;
		} else {
			to_minus = 0.0;
		}

		error += (outputs[i] - to_minus).powf(2.0);

	}

	error / len as f32

}

impl Network {
	pub fn new(inputs: i32, length: i32, activation_type: ActivationType) -> Network {
		let mut n = Network {
			neurons: Vec::new(),
		};

		let mut init_neurons = Vec::new();

		for _i in 0..length {
			init_neurons.push(Neuron::new(activation_type.clone(), inputs))
		}

		n.neurons.push(init_neurons);

		n
	}

	pub fn add_layer(&mut self, length: i32, activation_type: ActivationType) {
		let mut add_neurons = Vec::new();
		let input_length = self.neurons.last().unwrap().len();

		for _i in 0..length {
			add_neurons.push(Neuron::new(activation_type.clone(), input_length as i32));
		}

		self.neurons.push(add_neurons);
	}

	fn activate(&mut self, inputs: &Vec<f32>) {
		//start by activating the first layer with provided inputs
		activate_layer(&mut self.neurons[0], inputs);
		//do the rest with the previous layers outputs
		let len = self.neurons.len();
		for i in 1..len {
			let outputs = outputs_layer(&self.neurons[i-1]);
			activate_layer(&mut self.neurons[i], &outputs);
		}
	}

	pub fn predict_from_input(&mut self, inputs: &Vec<f32>, 
    expected: i32) -> i32{
		self.activate(inputs);
		let outputs = outputs_layer(self.neurons.last().unwrap());

    let mut current = -50000000.0;
    let mut to_return = 0;
    let len = outputs.len();
    for i in 0..len {
      if outputs[i] > current {
        to_return = i as i32;
        current = outputs[i];
      }
    }
    
    to_return

	}

  fn dfa_exec_error(&self, error_type: ErrorType, expected: i32) -> Vec<f32> {
    
		let mut errors = outputs_layer(&self.neurons.last().unwrap());

    match error_type {
      ErrorType::MSE => {
        let len = errors.len();
        for i in 0..len {
          let minus;
          if i == expected as usize {
            minus = 1.0;
          } else {
            minus = 0.0;
          }
          errors[i] = (2.0 * (errors[i] - minus)) / len as f32;
        }

        errors
      },
    }
  }

	fn dfa_exec_line(&mut self, pos: usize, inputs: &Vec<f32>, errors: &Vec<f32>) {
		let line:&mut Vec<Neuron> = &mut self.neurons[pos];
		let len = line.len();
		for i in 0..len {
			line[i].dfa_exec(errors, inputs);
		}
	}

	pub fn dfa_exec(&mut self, inputs: &Vec<f32>, error_type: ErrorType, expected: i32) {
		self.activate(inputs);

		let errors = self.dfa_exec_error(error_type, expected);
		//do first layer
		self.dfa_exec_line(0, inputs, &errors);

		let len = self.neurons.len();
		for i in 1..len {
			let previous_layer_outputs = outputs_layer(&self.neurons[i-1]);
			self.dfa_exec_line(i, &previous_layer_outputs, &errors);
		}
	}

  pub fn dfa_learn(&mut self, learning: f32) {
    let neuron_len = self.neurons.len();
    
    for j in 0..neuron_len {
      let len = self.neurons[j].len();
      for i in 0..len {
        self.neurons[j][i].dfa_learn(learning);
      }
    }

  }

	pub fn debug_init(&self, file: &mut File, inputs: i32) {
		file.write(b"Error, ").unwrap();

		for i in 0..inputs {
			file.write_fmt(format_args!("Input {}, ", i)).unwrap()
		}
		let len = self.neurons.len();
		for i in 0..len {
			debug_init_layer(&self.neurons[i], file, i as i32)
		}

		file.write(b"\n").unwrap();
	}

  pub fn debug_init_min(&self, file: &mut File) {
    file.write(b"Error, ").unwrap();
    let len = self.neurons.last().unwrap().len();

    for i in 0..len {
      file.write_fmt(format_args!("Output {}, ", i)).unwrap();
    }

		file.write(b"\n").unwrap();
  }

	pub fn debug_out(&self, file: &mut File, inputs: &Vec<f32>, expected: i32, _error_type: ErrorType) {
		//error
		let outputs  = outputs_layer(&self.neurons.last().unwrap());
		file.write_fmt(format_args!("{}, ", mse_error(&outputs, expected))).unwrap();
		let mut len = inputs.len();

		for i in 0..len {
			file.write_fmt(format_args!("{}, ", inputs[i])).unwrap();
		}
		len = self.neurons.len();
		for i in 0..len {
			debug_out_layer(&self.neurons[i], file);
		}

		file.write(b"\n").unwrap();

	}

  pub fn debug_out_min(&self, file: &mut File, expected: i32, _error_type: ErrorType) {
    let outputs = outputs_layer(&self.neurons.last().unwrap());
    file.write_fmt(format_args!("{}, ", mse_error(&outputs, expected))).unwrap();
    let len = outputs.len();

    for i in 0..len {
      if i == expected as usize {
        file.write_fmt(format_args!("E: {}, ", outputs[i])).unwrap();
      } else {
        file.write_fmt(format_args!("{}, ", outputs[i])).unwrap();
      }
    }

		file.write(b"\n").unwrap();
  }
}
