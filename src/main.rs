extern crate ocl;

use crate::network::{Network, ErrorType};
use crate::network::neuron::ActivationType;
use crate::data::{Image, Dataset};
use std::fs::File;

//use std::env;
//use std::path::Path;

mod data;
mod network;

fn check_data(d: &Dataset, n: &mut Network) {

    let mut correct = 0;
    for i in 0..10 {
      let answer = n.predict_from_input(&d.testing[i].pixels, 
        d.testing[i].label);
      if answer == d.testing[i].label {
        correct += 1;
      }
    }
    println!("Correct: {}, {}%", correct, correct as f32 / 10 as f32);
}

fn main() {
    //let root = Path::new("./bin");
    //env::set_current_dir(&root);
    let mut inputs: Vec<Vec<f32>> = Vec::new();
    inputs.push(vec![0.0, 0.0]);
    inputs.push(vec![0.0, 1.0]);
    inputs.push(vec![1.0, 0.0]);
    inputs.push(vec![1.0, 1.0]);

    let answers = vec![1,0,0,1];

    let d = Dataset::new(); 

    let mut file = File::create("output.csv").unwrap();
    let mut n = Network::new(784, 1000, ActivationType::SIGMOID);
    n.add_layer(1000, ActivationType::SIGMOID);
    n.add_layer(10, ActivationType::SIGMOID);
    n.debug_init_min(&mut file);

    //n.predict_from_input(&d.training[0].pixels);
    //n.debug_out_min(&mut file, 1, ErrorType::MSE);
    /*let mut correct = 0;
    for i in 0..10000 {
      let answer = n.predict_from_input(&d.testing[i].pixels, 
        d.testing[i].label);
      if answer == d.testing[i].label {
        correct += 1;
      }
    }*/

    check_data(&d, &mut n);


    for _i in 0..1000 {
        for j in 0..10 {
            n.dfa_exec(&d.testing[7].pixels, ErrorType::MSE, d.testing[7].label);
            /*if j % 5 == 0 {
              n.dfa_learn(0.01);
              //n.debug_out_min(&mut file, d.testing[j].label, ErrorType::MSE);
            }*/
            if j == 7 {
              n.debug_out_min(&mut file, d.testing[7].label, ErrorType::MSE);
            }
        }
        n.dfa_learn(0.01);
        //n.debug_out_min(&mut file, answers[3], ErrorType::MSE);
    }
    //let len = d.training[0].pixels.len();
    //for i in 0..len {
    //  println!("{}", d.training[0].pixels[i]);
    //}
    //
    check_data(&d, &mut n);

    //println!("Hello, world!");
}

